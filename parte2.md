
# Continuación del Tutorial de Django: Creando un Modelo "Inventario"

En esta parte del tutorial, vamos a crear un modelo sencillo llamado "Inventario" y lo habilitaremos en el administrador de Django para que puedas agregar, editar y borrar registros desde la interfaz de administración.

## Paso 7: Crear el Modelo Inventario

Los modelos en Django son la definición de las tablas de la base de datos desde el código. Vamos a definir un modelo `Inventario` que representará los items que queremos mantener en nuestro inventario.

1. Abre el archivo `models.py` dentro del directorio de tu aplicación `saludo` (o el nombre que le hayas dado a tu aplicación) y agrega la siguiente clase:

```python
# saludo/models.py
from django.db import models

class Inventario(models.Model):
    nombre = models.CharField(max_length=100)
    descripcion = models.TextField()
    cantidad = models.IntegerField()

    def __str__(self):
        return self.nombre
```

Esta clase define un modelo con tres campos: `nombre`, `descripcion` y `cantidad`. El método `__str__` se usa para representar el objeto como una cadena, que es útil, por ejemplo, en el panel de administración.

## Paso 8: Migraciones

Después de definir el modelo, necesitamos decirle a Django que cree la tabla correspondiente en la base de datos. Esto se hace a través de las migraciones. Ejecuta los siguientes comandos en tu terminal:

```bash
python manage.py makemigrations
python manage.py migrate
```

El primer comando genera un archivo de migración basado en los cambios que hiciste en `models.py`, y el segundo aplica la migración a la base de datos, creando efectivamente la tabla.

## Paso 9: Registrar el Modelo en el Admin de Django

Para poder gestionar nuestro modelo `Inventario` desde el panel de administración de Django, necesitamos registrar el modelo. Abre el archivo `admin.py` en el directorio de tu aplicación y modifícalo para incluir lo siguiente:

```python
# saludo/admin.py
from django.contrib import admin
from .models import Inventario

admin.site.register(Inventario)
```

## Paso 10: Crear un Superusuario

Para acceder al panel de administración, necesitas tener un usuario administrador. Crea uno ejecutando:

```bash
python manage.py createsuperuser
```

Sigue las instrucciones en la terminal para configurar el nombre de usuario y la contraseña.

## Paso 11: Acceder al Panel de Administración

Inicia el servidor de desarrollo si no está en ejecución:

```bash
python manage.py runserver
```

Abre un navegador y ve a `http://127.0.0.1:8000/admin/`. Inicia sesión con el usuario y contraseña que acabas de crear. Deberías ver el modelo `Inventario` listado en el panel de administración, y podrás empezar a agregar, editar y eliminar elementos de tu inventario.

## Conclusión

Has expandido tu aplicación web Django para incluir un modelo de datos, lo has aplicado a través de migraciones, y has hecho que sea gestionable a través del panel de administración de Django. ¡Excelente trabajo!


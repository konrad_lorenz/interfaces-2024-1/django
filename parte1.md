
# Tutorial de Django: ¡Hola, clase de estructuras KL!

Crear una aplicación web con Django que muestre "¡Hola, clase de estructuras!" en un navegador es un excelente ejercicio para empezar con uno de los frameworks de desarrollo web más populares de Python. Sigue estos pasos para lograrlo.

## Paso 1: Instalación de Django

Asegúrate de tener Python instalado en tu sistema. Django se puede instalar fácilmente usando pip, el gestor de paquetes de Python. En tu terminal o línea de comandos, ejecuta:

```bash
pip install django
```

## Paso 2: Crear un proyecto Django

Una vez instalado Django, el siguiente paso es crear un nuevo proyecto. Desde la terminal, navega al directorio donde deseas crear tu proyecto y ejecuta:

```bash
django-admin startproject mi_proyecto
```

Esto creará un nuevo directorio `mi_proyecto` con la estructura básica de un proyecto Django.

## Paso 3: Crear una aplicación Django

Un proyecto Django puede contener múltiples aplicaciones, cada una realizando una función específica. Para crear una aplicación, navega al directorio del proyecto y ejecuta:

```bash
cd mi_proyecto
python manage.py startapp saludo
```

Esto crea una nueva aplicación llamada `saludo` dentro de tu proyecto.

## Paso 4: Definir una vista

Las vistas en Django son funciones de Python que toman una solicitud web y devuelven una respuesta. Abre el archivo `views.py` dentro del directorio de tu aplicación `saludo` y define una vista que devuelva nuestro mensaje. Añade el siguiente código:

```python
# saludo/views.py
from django.http import HttpResponse

def hola_clase(request):
    return HttpResponse("¡Hola, clase de estructuras!")
```

## Paso 5: Configurar la URL

Para que esta vista sea accesible, necesitas mapearla a una URL. Primero, crea un archivo `urls.py` dentro del directorio de tu aplicación `saludo` si no existe y agrega el siguiente código:

```python
# saludo/urls.py
from django.urls import path
from .views import hola_clase

urlpatterns = [
    path('', hola_clase),
]
```

Luego, necesitas incluir estas URLs de la aplicación en las URLs del proyecto principal. Abre el archivo `urls.py` en el directorio `mi_proyecto/mi_proyecto` y modifícalo para incluir las URLs de la aplicación `saludo`:

```python
# mi_proyecto/urls.py
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('saludo/', include('saludo.urls')),
]
```

## Paso 6: Ejecutar el servidor de desarrollo

Ahora que has configurado tu vista y URL, es hora de ejecutar el servidor de desarrollo y ver tu aplicación en acción. Ejecuta el siguiente comando desde el directorio raíz de tu proyecto:

```bash
python manage.py runserver
```

Abre tu navegador y ve a `http://127.0.0.1:8000/saludo/`. Deberías ver el mensaje "¡Hola, clase de estructuras!".

## Conclusión

¡Felicidades! Has creado una simple aplicación web con Django que muestra un mensaje en el navegador. Este tutorial cubre los fundamentos de Django, incluyendo la instalación, creación de proyectos y aplicaciones, definición de vistas, configuración de URLs y ejecución del servidor de desarrollo. Django es un framework poderoso y versátil, y este ejercicio es solo el comienzo de lo que puedes hacer con él.


# Integración de Django con Plotly: Visualización de Datos de Inventario

En esta última parte del tutorial, te mostraremos cómo integrar Plotly con Django para visualizar los datos de tu modelo `Inventario` con gráficos interactivos.

## Paso 12: Instalar Plotly

Primero, necesitas instalar Plotly en tu entorno. Plotly es una biblioteca de gráficos interactivos de Python que puedes usar para crear diversas visualizaciones de datos. Ejecuta el siguiente comando en tu terminal:

```bash
pip install plotly
```

## Paso 13: Preparar los Datos para Plotly

Para visualizar los datos de inventario, primero debemos obtenerlos desde nuestro modelo `Inventario`. En este ejemplo, vamos a crear una visualización simple de la cantidad de cada item en el inventario.

1. Abre el archivo `views.py` de tu aplicación y agrega lo siguiente:

```python
# Importar Plotly y Django HttpResponse
import plotly.graph_objs as go
from plotly.offline import plot
from django.http import HttpResponse
from .models import Inventario

def grafico_inventario(request):
    # Obtener datos del modelo Inventario
    inventario = Inventario.objects.all()
    nombres = [item.nombre for item in inventario]
    cantidades = [item.cantidad for item in inventario]

    # Crear el gráfico
    trace = go.Bar(x=nombres, y=cantidades)
    layout = go.Layout(title='Cantidad de Items en Inventario', xaxis=dict(title='Items'), yaxis=dict(title='Cantidad'))
    fig = go.Figure(data=[trace], layout=layout)

    # Renderizar el gráfico en HTML
    div = plot(fig, output_type='div')

    return HttpResponse(div)
```

## Paso 14: Crear una URL para la Visualización

Ahora necesitas crear una URL para acceder a tu visualización. Añade una nueva ruta en el archivo `urls.py` de tu aplicación:

```python
# saludo/urls.py
from django.urls import path
from .views import grafico_inventario

urlpatterns = [
    path('grafico/', grafico_inventario),
]
```

## Paso 15: Visualizar los Datos

Inicia el servidor de desarrollo si no está en ejecución y abre tu navegador:

```bash
python manage.py runserver
```

Ve a `http://127.0.0.1:8000/saludo/grafico/`. Deberías ver un gráfico de barras mostrando la cantidad de cada item en tu inventario.

## Conclusión

¡Felicidades! Ahora has integrado Plotly con Django para visualizar los datos de tu modelo `Inventario` con gráficos interactivos. Esto marca el final de nuestro tutorial básico de Django, pero solo es el comienzo de lo que puedes hacer con Django y Plotly para la visualización de datos.
